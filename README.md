---
layout: page
title: Strupler2011a--Karumzeitlichen_Keramikinventare
---

Source of the preprint: 

**Strupler 2011** “Vorläufiger Überblick über die Karum-zeitlichen Keramikinventare”, in A.
Schachner, Die Ausgrabungen in Boğazköy – Ḫattuša 2010, _Archäologischer Anzeiger_ 2011/1, 51–57.

 [preprint : https://hal.archives-ouvertes.fr/hal-01483327](https://hal.archives-ouvertes.fr/hal-01483327)

## Licence

  - Text: CC BY-SA (http://creativecommons.org/licenses/by-sa/4.0/)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"></span> <a xmlns:cc="http://creativecommons.org/ns#" href= "https://hal.archives-ouvertes.fr/hal-01483327" property="cc:attributionName" rel="cc:attributionURL"> <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"></a>
